;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages gothic-2)
  #:use-module (games gog-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages wine)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (nonguix licenses))

(define-public gog-gothic-2
  (package
    (name "gog-gothic-2")
    (version "2.7")
    (source #f)
    (native-inputs
      `(("innoextract" ,innoextract)
        ("unzip" ,unzip)
        ("imagemagick" ,imagemagick)
        ("gog-gothic-2.exe"
         ,(origin
            (method gog-fetch)
            (uri "gogdownloader://gothic_2_gold_edition/en1installer0")
            (file-name (string-append "gog-gothic-2-" version ".exe"))
            (sha256 (base32 "1a6lcy8ly0l9wi080dxdkahpf8a53h06c7fcj4sah5msczfmgdh9"))))
        ("gog-gothic-2-1.bin"
         ,(origin
            (method gog-fetch)
            (uri "gogdownloader://gothic_2_gold_edition/en1installer1")
            (file-name (string-append "gog-gothic-2-" version "-1.bin"))
            (sha256 (base32 "0sw67rmlbas41wxr0pkbyyxmqvkng6iri5bjf5az1ylfhg0880yy"))))
        ("gog-gothic-2-icons.zip"
         ,(origin
            (method gog-fetch)
            (uri "gogdownloader://gothic_2_gold_edition/5113")
            (file-name (string-append "gog-gothic-2-icons.zip"))
            (sha256 (base32 "0lk5zk69p4p8irjrzsy79fk0d43dq2v3iqjyln1120zybp8qzlgl"))))))
    (inputs
      (list coreutils wine))
    (build-system copy-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          (replace 'unpack
            (lambda* (#:key inputs #:allow-other-keys)
              (let ((gog-gothic-2.exe (assoc-ref inputs "gog-gothic-2.exe"))
                    (gog-gothic-2-1.bin (assoc-ref inputs "gog-gothic-2-1.bin"))
                    (gog-gothic-2-icons.zip (assoc-ref inputs "gog-gothic-2-icons.zip")))
                (symlink gog-gothic-2.exe "gog-gothic-2.exe")
                (symlink gog-gothic-2-1.bin "gog-gothic-2-1.bin")
                (invoke "innoextract"
                        "--lowercase"
                        "--include" "/data"
                        "--include" "/miles"
                        "--include" "/system"
                        "--include" "/_work"
                        "--include" "/vdfs.cfg"
                        "--include" "/manual.pdf"
                        "--include" "/manual_addon.pdf"
                        "gog-gothic-2.exe")
                (invoke "unzip" gog-gothic-2-icons.zip "-d" "icons"))))
          (add-after 'unpack 'convert-icons
            (lambda _
              (invoke "convert" "icons/Dragon.jpg" "icons/Dragon.png")
              (invoke "convert" "icons/Dragon_Slayer.jpg" "icons/Dragon_Slayer.png")
              (invoke "convert" "icons/Logo.jpg" "icons/Logo.png")
              (invoke "convert" "icons/Paladin.jpg" "icons/Paladin.png")))
          (add-after 'install 'install-wrapper
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let ((coreutils (assoc-ref inputs "coreutils"))
                    (wine (assoc-ref inputs "wine"))
                    (out (assoc-ref outputs "out")))
                (mkdir-p (string-append out "/bin"))
                (call-with-output-file (string-append out "/bin/gothic2")
                  (lambda (port)
                    (display
                      (string-append
                        "#!/bin/sh\n\n"

                        "export XDG_DATA_HOME=\"${XDG_DATA_HOME:-$HOME/.local/share}\"\n"
                        "export WINEPREFIX=\"$XDG_DATA_HOME/gothic2/wine\"\n"
                        "export WINEDLLOVERRIDES=\"mscoree=\"\n"
                        "export PATH=\"" coreutils "/bin:" wine "/bin\"\n\n"

                        "sourcedir=\"" out "/share/gothic2\"\n"
                        "targetdir=\"$XDG_DATA_HOME/gothic2\"\n"
                        "mkdir -p -m=755 \"$targetdir\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/system\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/system\"\n"
                        "  elif [ \"$i\" = \"$sourcedir/_work\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/_work\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$sourcedir/system\"\n"
                        "targetdir=\"$targetdir/system\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/gothic.ini\" ]; then\n"
                        "    if [ ! -f \"$targetdir/gothic.ini\" ]; then\n"
                        "      install -m=644 \"$i\" \"$targetdir/\"\n"
                        "    fi\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$(dirname \"$sourcedir\")/_work\"\n"
                        "targetdir=\"$(dirname \"$targetdir\")/_work\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/data\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/data\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$sourcedir/data\"\n"
                        "targetdir=\"$targetdir/data\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/presets\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/presets\"\n"
                        "  elif [ \"$i\" = \"$sourcedir/scripts\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/scripts\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$sourcedir/presets\"\n"
                        "targetdir=\"$targetdir/presets\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/dialogcams.zen\" ]; then\n"
                        "    install -C -m=444 \"$i\" \"$targetdir/\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$(dirname \"$sourcedir\")/scripts\"\n"
                        "targetdir=\"$(dirname \"$targetdir\")/scripts\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/content\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/content\"\n"
                        "  elif [ \"$i\" = \"$sourcedir/_compiled\" ]; then\n"
                        "    mkdir -p -m=755 \"$targetdir/_compiled\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$sourcedir/content\"\n"
                        "targetdir=\"$targetdir/content\"\n"
                        "mkdir -p -m=755 \"$targetdir/cutscene\"\n"
                        "for i in \"$sourcedir/cutscene\"/*; do\n"
                        "  if [ \"$i\" = \"$sourcedir/cutscene/ou.lsc\" ]; then\n"
                        "    install -C -m=444 \"$i\" \"$targetdir/cutscene/\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/cutscene/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "sourcedir=\"$(dirname \"$sourcedir\")/_compiled\"\n"
                        "targetdir=\"$(dirname \"$targetdir\")/_compiled\"\n"
                        "for i in \"$sourcedir\"/*; do\n"
                        "  if [ ! \"$i\" = \"$sourcedir/ouinfo.inf\" ]; then\n"
                        "    install -C -m=444 \"$i\" \"$targetdir/\"\n"
                        "  else\n"
                        "    ln -s -f \"$i\" \"$targetdir/\"\n"
                        "  fi\n"
                        "done\n\n"

                        "wine \"$XDG_DATA_HOME/gothic2/system/gothic2.exe\"\n")
                      port)))
                (chmod (string-append out "/bin/gothic2") #o555))))
          (add-after 'install-wrapper 'install-desktop-entry
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
                (make-desktop-entry-file
                  (string-append out "/share/applications/gothic2.desktop")
                  #:name "Gothic II"
                  #:icon (string-append out
                           "/share/icons/hicolor/150x150/apps/gothic2-logo.png")
                  #:exec (string-append out "/bin/gothic2")
                  #:categories '("Application" "Game"))))))
        #:install-plan
        '(("data" "share/gothic2/")
          ("miles" "share/gothic2/")
          ("system" "share/gothic2/")
          ("_work" "share/gothic2/")
          ("vdfs.cfg" "share/gothic2/")
          ("manual.pdf" "share/doc/gothic2/")
          ("manual_addon.pdf" "share/doc/gothic2/manual-addon.pdf")
          ("icons/Dragon.png"
           "share/icons/hicolor/150x150/apps/gothic2-dragon.png")
          ("icons/Dragon_Slayer.png"
           "share/icons/hicolor/150x150/apps/gothic2-dragon-slayer.png")
          ("icons/Logo.png"
           "share/icons/hicolor/150x150/apps/gothic2-logo.png")
          ("icons/Paladin.png"
           "share/icons/hicolor/150x150/apps/gothic2-paladin.png"))))
    (home-page "https://gog.com/game/gothic_2_gold_edition")
    (synopsis "Gothic II")
    (description "Gothic II is a role-playing video game developed by a German company
Piranha Bytes and the sequel to Gothic I.  It was released on November 29, 2002 in Germany
and in North America on October 28, 2003.

Gothic II was a commercial success in Germany, and became JoWood's biggest hit at the time
of its release.  By 2004, it had sold over 300,000 copies when combined with its expansion
pack, Night of the Raven.

The events of Gothic II follow shortly after the finale of the original game.  When the
barrier around the prison colony was destroyed, ore supplies for the kingdom have stopped.
The king decides to send Lord Hagen with 100 paladins to the island to secure ore.  On
Khorinis, prisoners that escaped the camp raided the country.  Seeing as the militia was
unable to protect them, some farmers formed an alliance with the refugees and no longer
paid allegiance to the king.  Evil did not disappear with the Sleeper being banned, as
with his last cry, the Sleeper summoned the most evil creatures.  Xardas felt this and
rescued the Nameless Hero from under the ruins of the Sleeper's temple, where he had lain
for weeks, becoming weak.")
    (license (undistributable "No URL"))))
