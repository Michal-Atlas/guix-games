;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages tesseract)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix svn-download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages sdl))

;; While the engine is free software, the license of the game assets is unclear.
;; The parent project Cube 2: Sauerbraten has non-free assets, but it's
;; unclear whether Tesseract uses any of them.
;; See bug #41967 for a discussion about including it upstream.

(define-public tesseract
  (let ((svn-revision 2411))
    (package
      (name "tesseract")
      (version (string-append "20200615-" (number->string svn-revision)))
      (source
       (origin
         (method svn-fetch)
         (uri (svn-reference
               (url "svn://svn.tuxfamily.org/svnroot/tesseract/main")
               (revision svn-revision)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1av9jhl2ivbl7wfszyhyna84llvh1z2d8khkmadm8d105addj10q"))
         (modules '((guix build utils)))
         (snippet
          '(begin
             (for-each delete-file-recursively
                       '("bin" "bin64"
                         "server.bat"
                         "tesseract.bat"
                         "src/lib"
                         "src/lib64"))
             #t))))
      (build-system gnu-build-system)
      (arguments
       `(#:make-flags (list "CC=gcc")
         #:tests? #f                    ; No tests.
         #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (add-after 'unpack 'cd-src
             (lambda _ (chdir "src") #t))
           (add-before 'build 'fix-env
             (lambda* (#:key inputs #:allow-other-keys)
               (setenv "CPATH"
                       (string-append (assoc-ref inputs "sdl2-union")
                                      "/include/SDL2:"
                                      (or (getenv "CPATH") "")))
               #t))
           (add-after 'install 'really-install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/tesseract"))
                      (bin (string-append out "/bin/tesseract"))
                      (client (string-append out "/bin/tesseract-client")))
                 (chdir "..")           ; Back to root.
                 (for-each
                  (lambda (dir)
                    (mkdir-p (string-append share "/" dir))
                    (copy-recursively dir (string-append share "/" dir)))
                  '("config" "media"))
                 (mkdir-p (string-append out "/bin/"))
                 (copy-file "bin_unix/native_client" client)
                 (copy-file "bin_unix/native_server"
                            (string-append out "/bin/tesseract-server"))
                 (call-with-output-file bin
                   (lambda (p)
                     (format p "#!~a
TESS_DATA=~a
TESS_BIN=~a
TESS_OPTIONS=\"-u$HOME/.tesseract\"
cd \"$TESS_DATA\"
exec \"$TESS_BIN\" \"$TESS_OPTIONS\" \"$@\""
                             (which "bash")
                             share
                             client)))
                 (chmod bin #o755)
                 (install-file "src/readme_tesseract.txt"
                               (string-append out "/share/licenses/tesseract/LICENSE"))
                 (let ((icon (string-append out "/share/pixmaps/tesseract.png")))
                   (mkdir-p (dirname icon))
                   (copy-file "media/interface/cube.png" icon)
                   (make-desktop-entry-file
                    (string-append out "/share/applications/tesseract.desktop")
                    #:name "Tesseract"
                    #:comment "First-person shooter with map editing"
                    #:exec "tesseract"
                    #:icon icon
                    #:categories '("Game"))))
               #t)))))
      (inputs
       `(("sdl2-union" ,(sdl-union (list sdl2 sdl2-mixer sdl2-image)))
         ("zlib" ,zlib)
         ("libpng" ,libpng)
         ("libgl" ,mesa)))
      (home-page "http://tesseract.gg/")
      (synopsis "First-person shooter with map editing, instagib, DM and CTF")
      (description "Tesseract is a first-person shooter game focused on
instagib deathmatch and capture-the-flag gameplay as well as cooperative
in-game map editing.

Tesseract provides a free software engine derived from @emph{Cube 2:
Sauerbraten} technology but with upgraded modern rendering techniques.  The
new rendering features include fully dynamic omnidirectional shadows, global
illumination, HDR lighting, deferred shading, morphological / temporal /
multisample anti-aliasing, and much more.")
      ;; TODO: Engine is zlib, but what about the assets?
      (license license:zlib))))
