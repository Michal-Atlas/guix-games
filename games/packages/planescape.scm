(define-module (games packages planescape)
  #:use-module (guix packages)
  #:use-module (games gog-download)
  #:use-module (games build-system mojo)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (past packages tls)
  #:use-module (nonguix licenses)
  #:use-module (guix transformations))

(define-public gog-planescape
  (package
    (name "gog-planescape")
    (version "3.1.4")
    (source
     (origin
       (method gog-fetch)
       (uri "gogdownloader://planescape_torment_enhanced_edition_game/en3installer0")
       (file-name (string-append "gog-planescape-torment-" version ".sh"))
       (sha256 (base32 "1plil37525l20j1fpk8726v6vh8rsny2x06msvd2q0900j8xlbl1"))))
    (arguments `(#:patchelf-plan
                 (map (lambda (binary)
                        (list binary
                              '("gcc:lib"
                                "openal"
                                "mesa"
                                "openssl"
                                "expat"
                                "libX11")))
                      '("Torment64"
                        "Torment"))))
    (inputs `(("gcc:lib" ,gcc "lib")
              ("openal" ,openal)
              ("mesa" ,mesa)
              ("openssl" ,((options->transformation
                            `((without-tests . "openssl")))
                           openssl-1.0))
              ("expat" ,expat)
              ("libX11" ,libx11)))
    (build-system mojo-build-system)
    (home-page "https://www.gog.com/game/planescape_torment_enhanced_edition")
    (synopsis "Planescape: Torment: Enhanced Edition")
    (description "The original Planescape: Torment was released in 1999 to widespread critical acclaim. It won RPG of the Year from multiple outlets for its unconventional story, characters, and amazing soundtrack. Since then, millions of Planescape: Torment fans have enjoyed exploring the strange and dangerous city of Sigil and surrounding planes through the Nameless One's eyes.")
    (license (undistributable "No URL"))))
