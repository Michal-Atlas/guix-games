;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages world-of-goo)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (games build-system mojo)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games gog-download)
  #:use-module (games humble-bundle))

(define-public world-of-goo
  (let* ((version "1.53")
         (file-name (string-append "WorldOfGoo.Linux." version ".sh"))
         (binary (if (target-64bit?)
                   "data/x86_64/WorldOfGoo.bin.x86_64"
                   "data/x86/WorldOfGoo.bin.x86")))
    (package
      (name "world-of-goo")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "1vjkglc75imyasf78cc2vvhsmy19a3b4pnj295bg2rd7k424nphp"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f       ; allocated section `.dynstr' not in segment
         #:patchelf-plan
         `((,,binary
            ("libc" "sdl2" "sdl2-mixer" "libvorbis" "libogg" "gcc" "mesa" "glu")))
         #:install-plan
         `((,,binary #f ,,(string-append "share/world-of-goo/" (basename binary)))
           ("data/noarch/game/properties" (".*") "share/world-of-goo/game/properties/")
           ("data/noarch/game/res" (".*") "share/world-of-goo/game/res/")
           ("data/noarch/game" ("gooicon.png") "share/pixmaps/world-of-goo.png")
           ("data" ("eula.txt") "share/doc/world-of-goo/")
           ("data/noarch" ("readme.html") "share/doc/world-of-goo/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "makeself_safeextract")
                       "--mojo"
                       (assoc-ref inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/" "world-of-goo"))
                      (real (string-append output "/share/world-of-goo/" ,(basename binary))))
                 (make-wrapper wrapper real)
                 (make-desktop-entry-file (string-append output "/share/applications/world-of-goo.desktop")
                                          #:name "World of Goo"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/world-of-goo.png")
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("makeself-safeextract" ,makeself-safeextract)))
      (inputs
       `(("mesa" ,mesa)
         ("glu" ,glu)
         ("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("sdl2" ,sdl2)
         ("sdl2-mixer" ,sdl2-mixer)
         ("libvorbis" ,libvorbis)
         ("libogg" ,libogg)))
      (home-page "https://2dboy.com/")
      (synopsis "Physics based puzzle / construction game")
      (description "World of Goo is an award winning a physics based puzzle /
construction game.  The millions of Goo Balls who live in the beautiful World of
Goo don't know that they are in a game, or that they are extremely delicious.")
      (license (undistributable "No URL")))))

(define-public gog-world-of-goo
  (let ((buildno "29337")
        (binary (if (target-64bit?)
                  "data/x86_64/WorldOfGoo.bin.x86_64"
                  "data/x86/WorldOfGoo.bin.x86")))
    (package
      (inherit world-of-goo)
      (name "gog-world-of-goo")
      (version "1.51")
      (source
       (origin
        (method gog-fetch)
        (uri "gogdownloader://world_of_goo/en3installer0")
        (file-name (string-append "world_of_goo_"
                                  (string-replace-substring version "." "_")
                                  "_" buildno ".sh"))
        (sha256
         (base32
          "01bhwnlgpkrjz6sb72z8ci51pwb38b9bp4ifncw480022qlswya1"))))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "sdl2" "sdl2-mixer" "libvorbis" "libogg" "gcc" "mesa" "glu")))))
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
